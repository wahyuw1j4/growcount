const { auth } = require("./auth");

const itemNotExist = (item) => {
  return {
    status: 202,
    message: `item ${item} not exist`,
  };
};

const read = (req, res) => {
  let ref = auth().ref("/items");
  ref.once("value", (snapshot) => {
    let data = snapshot.val();
    res.send(data);
  });
};

function findItems(itemName, callback) {
  auth()
    .ref("items")
    .orderByChild("name")
    .equalTo(itemName)
    .once("value", (snapshot) => {
      const itemExist = snapshot.val();
      callback(!!itemExist);
    });
}

let find = (req, res) => {
  const { itemName } = req.body;
  findItems(itemName, (result) => {
    if (result) {
      auth()
        .ref("items")
        .orderByChild("name")
        .equalTo(itemName)
        .once("value", (snapshot) => {
          let data = snapshot.val();
          res.status(200).json({
            status: 200,
            message: `item ${itemName} exist`,
            data: data,
          });
        });
    } else {
      res.status(202).json(itemNotExist(itemName));
    }
  });
};

const addItem = (req, res) => {
  const { itemName } = req.body;
  findItems(itemName, (result) => {
    if (result) {
      res.status(202).json({
        status: 202,
        message: `item ${itemName} exist`,
      });
    } else {
      let ref = auth().ref("/items");
      ref.push().set(
        {
          name: itemName.toLowerCase(),
          alias: {
            0: itemName.toLowerCase(),
          },
        },
        (err) => {
          if (err) {
            res.status(500).json({
              message: err,
              status: 500,
              erroor: "Server trouble",
            });
          } else {
            res.status(200).json({
              status: 200,
              message: `success add item ${itemName}`,
            });
          }
        }
      );
    }
  });
};

const updateItem = (req, res) => {
  const { itemName, newName } = req.body;
  findItems(itemName, (result) => {
    if (result) {
      let ref = auth().ref("items").orderByChild("name").equalTo(itemName);
      ref.once("value", (snapshot) => {
        keyItem = Object.keys(snapshot.val())[0];
        ref = auth().ref(`items/${keyItem}`).update({
          name: newName.toLowerCase(),
        });
        if (!!ref) {
          res.status(200).json({
            status: 200,
            message: `success update item ${itemName}`,
          });
        } else {
          res.status(500).json({
            status: 500,
            message: `Server trouble`,
          });
        }
      });
    } else {
      res.status(202).json(itemNotExist(itemName));
    }
  });
};

const deleteItem = (req, res) => {
  const { itemName } = req.body;
  findItems(itemName, (result) => {
    if (result) {
      let ref = auth().ref("items").orderByChild("name").equalTo(itemName);
      ref.once("value", (snapshot) => {
        keyItem = Object.keys(snapshot.val())[0];
        ref = auth().ref(`items/${keyItem}`).remove();
        if (!!ref) {
          res.status(200).json({
            status: 200,
            message: `success delete item ${itemName}`,
          });
        } else {
          res.status(500).json({
            status: 500,
            message: `Server trouble`,
          });
        }
      });
    } else {
      res.status(202).json(itemNotExist(itemName));
    }
  });
};

const addItemAlias = (req, res) => {
  const { itemName, newAlias } = req.body;
  findItems(itemName, (result) => {
    if (result) {
      let ref = auth().ref("items").orderByChild("name").equalTo(itemName);
      ref.once("value", (snapshot) => {
        keyItem = Object.keys(snapshot.val())[0];
        ref = auth().ref(`items/${keyItem}/alias`);
        ref.once("value", (snapshot) => {
          let datas = snapshot.val();
          // res.send()
          if (!Object.values(datas).includes(newAlias)) {
            //cek apakah alias sudah ada atau belum
            ref.push().set(newAlias.toLowerCase(), (err) => {
              if (err) {
                res.status(500).json({
                  message: err,
                  status: 500,
                  erroo: "Server trouble",
                });
              } else {
                res.status(200).json({
                  status: 200,
                  message: `success add alias ${newAlias}`,
                });
              }
            });
          } else {
            res.status(202).json({
              status: 202,
              message: `alias ${newAlias} exist`,
            });
          }
        });
      });
    } else {
      res.status(202).json(itemNotExist(itemName));
    }
  });
};

const deleteItemAlias = (req, res) => {
  const { itemName, alias } = req.body;
  findItems(itemName, (result) => {
    if (result) {
      let ref = auth().ref("items").orderByChild("name").equalTo(itemName);
      ref.once("value", (snapshot) => {
        keyItem = Object.keys(snapshot.val())[0];
        ref = auth().ref(`items/${keyItem}/alias`);
        ref.once("value", (snapshot) => {
          let datas = snapshot.val();
          let keyAlias = Object.keys(datas).find((key) => datas[key] === alias);
          if (keyAlias) {
            ref.child(keyAlias).remove();
            res.status(200).json({
              status: 200,
              message: `success delete alias ${alias}`,
            });
          } else {
            res.status(202).json({
              status: 202,
              message: `alias ${alias} not exist`,
            });
          }
        });
      });
    } else {
      res.status(202).json(itemNotExist(itemName));
    }
  });
};

const test = (req, res) => {
  const { itemName, newAlias } = req.body;
  findItems(itemName, (result) => {
    if (result) {
      // let ref = auth().ref("items").child(itemName).child("alias");
      // let ref = auth().ref(`/items/${itemName}/alias`);
      // .once("value", (snapshot) => {
      //   const itemExist = snapshot.val();

      // });
      ref.once("value", (snapshot) => {
        let datas = snapshot.val();
        // res.send()s
        res.send(datas);
        // if (!Object.values(datas).includes(newAlias)) {
        //   //cek apakah alias sudah ada atau belum
        //   ref.push().set(newAlias.toLowerCase(), (err) => {
        //     if (err) {
        //       res.status(500).json({
        //         message: err,
        //         status: 500,
        //         erroor: "Server trouble",
        //       });
        //     } else {
        //       res.status(200).json({
        //         status: 200,
        //         message: `success add alias ${newAlias}`,
        //       });
        //     }
        //   });
        // } else {
        //   res.status(202).json({
        //     status: 202,
        //     message: `data ${newAlias} exist`,
        //   });
        // }
      });
    } else {
      res.status(202).json(itemNotExist(itemName));
    }
  });
};

module.exports = {
  read,
  addItem,
  updateItem,
  deleteItem,
  addItemAlias,
  deleteItem,
  deleteItemAlias,
  find,
  test,
};
