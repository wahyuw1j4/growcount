let admin = require("firebase-admin");
let serviceAccount = require("./keyServiceAccSDKFirestore.json");


const auth = () => {
  if (admin.apps.length === 0) {
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      databaseURL: process.env.RTDB_Url,
      databaseAuthVariableOverride: {
        token: process.env.RTDB_Rules_Token
      }
    });
  }  
    let db = admin.database();
    return db
}

module.exports = {
  auth
}