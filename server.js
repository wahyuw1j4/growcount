const express = require('express')
const bodyparser = require('body-parser')
const db = require('./src/db/RTDB')


require('dotenv').config();
const host = "127.0.0.1"
const app = express()
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended: true}))
const port = 3132

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/read', db.read)
app.post('/additem', db.addItem)
app.delete('/deleteitem', db.deleteItem)
app.patch('/updateitem', db.updateItem)
app.post('/additemalias', db.addItemAlias)
app.delete('/deleteitemalias', db.deleteItemAlias)
app.get('/find', db.find)



app.post("/test", db.test)



app.listen(port, () => {
  console.log(`Sistem Berjalan di http://${host}:${port}`)
})